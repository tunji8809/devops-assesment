provider "aws" {
  profile = "default"
  region  = "us-east-1"
}

####Create VPC #################
resource "aws_vpc" "kube_vpc" {
  cidr_block           = "10.0.0.0/16"
  enable_dns_support   = "true"
  enable_dns_hostnames = "true"
  enable_classiclink   = "false"
  instance_tenancy     = "default"
  tags = {
    Name = "kube_vpc"
  }
}

########### SUBNET ####################
resource "aws_subnet" "default_subnet_a" {
  vpc_id                  = aws_vpc.kube_vpc.id
  availability_zone       = "us-east-1a"
  map_public_ip_on_launch = "true"
  cidr_block              = cidrsubnet(aws_vpc.kube_vpc.cidr_block, 4, 1)
}


resource "aws_subnet" "default_subnet_b" {
  vpc_id                  = aws_vpc.kube_vpc.id
  map_public_ip_on_launch = "true"
  availability_zone       = "us-east-1b"
  cidr_block              = cidrsubnet(aws_vpc.kube_vpc.cidr_block, 2, 1)
}
#############internet gateway #########
resource "aws_internet_gateway" "demo-igw" {
  vpc_id = aws_vpc.kube_vpc.id
  tags = {
    Name = "demo-igw"
  }
}

resource "aws_route_table" "demo-public-crt" {
  vpc_id = aws_vpc.kube_vpc.id

  route {
    //associated subnet can reach everywhere
    cidr_block = "0.0.0.0/0"
    //CRT uses this IGW to reach internet
    gateway_id = aws_internet_gateway.demo-igw.id
  }

  tags = {
    Name = "demo-public-crt"
  }
}

resource "aws_route_table_association" "demo-crta-public-subnet-1" {
  subnet_id      = aws_subnet.default_subnet_a.id
  route_table_id = aws_route_table.demo-public-crt.id
}

### create ECR repository to store docker IMage


resource "aws_ecr_repository" "ecr" {
  name                 = "demoapp"
  image_tag_mutability = "MUTABLE"

  image_scanning_configuration {
    scan_on_push = true
  }
  tags = {
    name = "demoapp"
  }
}


resource "aws_ecs_task_definition" "demo_task" {
  family                   = "demo-tasK" # Naming our first task
  container_definitions    = <<DEFINITION
  [
    {
      "name": "demo-tasK",
      "image": "${aws_ecr_repository.ecr.repository_url}",
      "essential": true,
      "portMappings": [
        {
          "containerPort": 3000,
          "hostPort": 3000
        }
      ],
      "memory": 512,
      "cpu": 256
    }
  ]
DEFINITION
  requires_compatibilities = ["FARGATE"] # Stating that we are using ECS Fargate
  network_mode             = "awsvpc"    # Using awsvpc as our network mode as this is required for Fargate
  memory                   = 512         # Specifying the memory our container requires
  cpu                      = 256         # Specifying the CPU our container requires
  execution_role_arn       = aws_iam_role.ecsTaskExecutionRole.arn
}



resource "aws_iam_role" "ecsTaskExecutionRole" {
  name               = "ecsTaskExecutionRole"
  assume_role_policy = data.aws_iam_policy_document.assume_role_policy.json
}



data "aws_iam_policy_document" "assume_role_policy" {
  statement {
    actions = ["sts:AssumeRole"]

    principals {
      type        = "Service"
      identifiers = ["ecs-tasks.amazonaws.com"]
    }
  }
}




resource "aws_iam_role_policy_attachment" "ecsTaskExecutionRole_policy" {
  role       = aws_iam_role.ecsTaskExecutionRole.name
  policy_arn = "arn:aws:iam::aws:policy/service-role/AmazonECSTaskExecutionRolePolicy"
}

resource "aws_alb" "application_load_balancer" {
  name               = "test-lb-tf" # Naming our load balancer
  load_balancer_type = "application"
  subnets = [
    aws_subnet.default_subnet_a.id,
    aws_subnet.default_subnet_b.id
  ]
  # Referencing the security group
  security_groups = [aws_security_group.load_balancer_security_group.id]
}

# Creating a security group for the load balancer:
resource "aws_security_group" "load_balancer_security_group" {
  name        = "demo-lb"
  description = "Allow  inbound traffic"
  vpc_id      = aws_vpc.kube_vpc.id
  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"] # Allowing traffic in from all sources
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

resource "aws_lb_target_group" "target_group" {
  name        = "target-group"
  port        = 80
  protocol    = "HTTP"
  target_type = "ip"
  vpc_id      = aws_vpc.kube_vpc.id # Referencing the default VPC
}

resource "aws_lb_listener" "listener" {
  load_balancer_arn = aws_alb.application_load_balancer.arn # Referencing our load balancer
  port              = "80"
  protocol          = "HTTP"
  default_action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.target_group.arn # Referencing our tagrte group
  }
}

##########################################################
# AWS ECS-CLUSTER
#########################################################

resource "aws_ecs_cluster" "cluster" {
  name = "demo-cluster"
  tags = {
    name = "demoapp"
  }

}

resource "aws_ecs_service" "demo_service" {
  name            = "demo-service"                        # Naming our first service
  cluster         = aws_ecs_cluster.cluster.id            # Referencing our created Cluster
  task_definition = aws_ecs_task_definition.demo_task.arn # Referencing the task our service will spin up
  launch_type     = "FARGATE"
  desired_count   = 3 # Setting the number of containers to 3

  load_balancer {
    target_group_arn = aws_lb_target_group.target_group.arn # Referencing our target group
    container_name   = aws_ecs_task_definition.demo_task.family
    container_port   = 3000 # Specifying the container port
  }

  network_configuration {
    subnets          = [aws_subnet.default_subnet_a.id, aws_subnet.default_subnet_b.id]
    assign_public_ip = true # Providing our containers with public IPs
    security_groups  = [aws_security_group.service_security_group.id]
  }
}


resource "aws_security_group" "service_security_group" {
  name        = "demo-service"
  description = "Allow  inbound traffic"
  vpc_id      = aws_vpc.kube_vpc.id
  ingress {
    from_port = 0
    to_port   = 0
    protocol  = "-1"
    # Only allowing traffic in from the load balancer security group
    security_groups = [aws_security_group.load_balancer_security_group.id]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}
