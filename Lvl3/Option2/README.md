####### How to Use #############


This project contains Ansible Plabook and Terraform script .

Terraform script Provisioned:
- vpc
- subnet
- IGW
- Route table
- ECS
- ECR


Ansible Playbook deployment:

- log in to ECR repo
- Build Dockerfile / image
- Tag Image
- Push Image to ECR

NOTE : Dockerfile exit in the files Directory  Ansible/roles/create-env/files.



Login into jenkins Server http://ec2-34-239-164-175.compute-1.amazonaws.com:8080/job/assessment/
Username: admin
Password:


JENKINS VARIABLE REQUIRED

** Account ID
** REGION
